import { PrismaClient } from "@prisma/client"
const prisma = new PrismaClient();

async function main() {
const allUsers = await prisma.user.findMany({where:{id:1}})
console.log(allUsers)
}

main()
  .catch(e => {
    throw e
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
