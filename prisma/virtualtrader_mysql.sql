

CREATE TABLE `User` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `createdAt` TIMESTAMP NOT NULL DEFAULT now(),
  `updatedAt` TIMESTAMP NOT NULL DEFAULT now(),
  `fullname` VARCHAR(63)  NOT NULL,
  `socialId` VARCHAR(63) ,
  `email` VARCHAR(63) ,
  `password` VARCHAR(63),
  `loginType` INTEGER NOT NULL,
  `status` VARCHAR(63) DEFAULT 'active'
);

CREATE TABLE `AssetDefinition` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `createdAt` TIMESTAMP NOT NULL DEFAULT now(),
  `updatedAt` TIMESTAMP NOT NULL DEFAULT now(),
  `symbol` VARCHAR(15),
  `name` VARCHAR(63),
  `rank` INTEGER,
  `category` VARCHAR(15),
  `country` VARCHAR(31),
  `exchange` VARCHAR(31)
);

CREATE TABLE `AssetValue` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `createdAt` TIMESTAMP NOT NULL DEFAULT now(),
  `updatedAt` TIMESTAMP NOT NULL DEFAULT now(),
  `open` decimal(65,30) DEFAULT NULL,
  `high` decimal(65,30) DEFAULT NULL,
  `low` decimal(65,30) DEFAULT NULL,
  `price` decimal(65,30) NOT NULL,
  `changeNominal` decimal(65,30) DEFAULT NULL,
  `changePercentage` decimal(65,30) DEFAULT NULL,
  `definitionId` INTEGER DEFAULT NULL,
  `rank` INTEGER DEFAULT NULL,
  `t` TIMESTAMP NOT NULL,
  FOREIGN KEY (`definitionId`) REFERENCES AssetDefinition(id)
);

CREATE TABLE `Device` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `createdAt` TIMESTAMP NOT NULL DEFAULT now(),
  `apiLevel` int(11) DEFAULT NULL,
  `brand` VARCHAR(63),
  `buildNumber` VARCHAR(63),
  `carrier` VARCHAR(63),
  `countryCode` VARCHAR(63),
  `deviceId` VARCHAR(63),
  `languageCode` VARCHAR(63),
  `deviceName` VARCHAR(63),
  `deviceType` VARCHAR(63),
  `model` VARCHAR(63),
  `serialNumber` VARCHAR(63),
  `systemName` VARCHAR(63),
  `systemVersion` VARCHAR(63),
  `userAgent` VARCHAR(63),
  `uniqueId` VARCHAR(63),
  `version` VARCHAR(63),
  `userId` INTEGER DEFAULT NULL,
  FOREIGN KEY (`userId`) REFERENCES User(id)
);

CREATE TABLE Account (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `createdAt` TIMESTAMP NOT NULL DEFAULT now(),
  `updatedAt` TIMESTAMP NOT NULL DEFAULT now(),
  `amount` decimal(65,30) NOT NULL,
  `userId` INTEGER DEFAULT NULL,
  `definitionId` INTEGER DEFAULT NULL,
  FOREIGN KEY (`userId`) REFERENCES User(id),
  FOREIGN KEY (`definitionId`) REFERENCES AssetDefinition(id)
);

CREATE TABLE `Transaction` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `createdAt` TIMESTAMP NOT NULL DEFAULT now(),
  `fromAmount` decimal(65,30) NOT NULL,
  `toAmount` decimal(65,30) NOT NULL,
  `transactionType` int(11) NOT NULL,
  `fromAssetId` INTEGER DEFAULT NULL,
  `userId` INTEGER DEFAULT NULL,
  `toAssetId` INTEGER DEFAULT NULL,
  FOREIGN KEY (`fromAssetId`) REFERENCES AssetDefinition(id),
  FOREIGN KEY (`userId`) REFERENCES User(id),
  FOREIGN KEY (`toAssetId`) REFERENCES AssetDefinition(id)
);

CREATE TABLE `UserLog` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `createdAt` datetime(3) NOT NULL,
  `userId` INTEGER NOT NULL,
  `logType` int(11) DEFAULT NULL,
  `content` VARCHAR(63),
  `duration` INTEGER DEFAULT NULL,
  FOREIGN KEY (`userId`) REFERENCES User(id)
);

