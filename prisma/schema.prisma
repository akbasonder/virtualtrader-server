generator client {
  provider = "prisma-client-js"
  // binaryTargets = ["rhel-openssl-1.1.x","linux-musl"]
  // output   = "../prisma/generated/javascript"
}

 generator typegraphql {
  provider = "typegraphql-prisma"
   // binaryTargets = ["rhel-openssl-1.1.x"]
   // binaryTargets = ["native","debian-openssl-1.1.x"]
  output   = "../prisma/generated/type-graphql"
  emitTranspiledCode = true
  simpleResolvers = true
}

datasource db {
  provider = "postgresql"
  url = env("DATABASE_URL")
}

model Account {
  id              Int              @id @default(autoincrement())
  createdAt       DateTime         @default(now())
  updatedAt       DateTime         @updatedAt
  amount          Decimal
  averagePrice    Decimal
  userId          Int
  assetId         Int
  status          String       @default("active")
  Asset Asset           @relation("AssetAccount",fields: [assetId], references: [id])
  User  User            @relation("UserAccount",fields: [userId], references: [id])

  // @@index([assetId], name: "accountAssetId")
  @@index([userId], name: "accountUserId")
}

model Asset {
  id                                                   Int           @id @default(autoincrement())
  createdAt                                            DateTime      @default(now())
  updatedAt                                            DateTime      @updatedAt
  symbol                                               String        @unique
  name                                                 String?
  capitalName                                          String?
  rank                                                 Int?
  priority                                             Int           @default(10)
  category                                             String?
  country                                              String?
  exchange                                             String?
  open                                                 Decimal?
  high                                                 Decimal?
  low                                                  Decimal?
  price                                                Decimal         @default(0)
  ask                                                  Decimal?
  bid                                                  Decimal?
  changeNominal                                        Decimal?
  changePercentage                                     Decimal?
  timestamp                                            DateTime?      @default(dbgenerated())
  Account                                              Account[]  @relation("AssetAccount")
  FromTransaction Transaction[] @relation("AssetToTransaction_fromAssetId")
  ToTransaction   Transaction[] @relation("AssetToTransaction_toAssetId")
  FromOrder Order[] @relation("AssetToOrder_fromAssetId")
  ToOrder   Order[] @relation("AssetToOrder_toAssetId")
  AssetHistory   AssetHistory[] @relation("AssetToHistory")
  Favorite      Favorite[]  @relation("AssetFavorite")
}

model Device {
  id            Int      @id @default(autoincrement())
  createdAt     DateTime @default(now())
  apiLevel      Int?
  brand         String?
  buildNumber   String?
  carrier       String?
  countryCode   String?
  deviceId      String?
  languageCode  String?
  deviceName    String?
  deviceType    String?
  model         String?
  serialNumber  String?
  systemName    String?
  systemVersion String?
  userAgent     String?
  uniqueId      String?
  version       String?
  userId        Int
  User          User    @relation("UserDevice",fields: [userId], references: [id])

  @@index([userId], name: "deviceUserId")
}

model Transaction {
  id                                                       Int              @id @default(autoincrement())
  createdAt                                                DateTime         @default(now())
  //fromUnitPrice                                            Float
  //toUnitPrice                                              Float
  isSell                                                   Boolean          @default(false)
  fromAmount                                               Decimal
  toAmount                                                 Decimal
  transactionType                                          Int
  status                                                   String           @default("completed")
  fromAssetId                                              Int
  userId                                                   Int
  toAssetId                                                Int
  FromAsset Asset @relation("AssetToTransaction_fromAssetId", fields: [fromAssetId], references: [id])
  ToAsset   Asset @relation("AssetToTransaction_toAssetId", fields: [toAssetId], references: [id])
  User      User  @relation("UserTransaction",fields: [userId], references: [id])

  // @@index([fromAssetId], name: "transactionFromAssetId")
  // @@index([toAssetId], name: "transactionToAssetId")
  @@index([userId], name: "transactionToUserId")
}

model Order {
  id                                                       Int              @id @default(autoincrement())
  createdAt                                                DateTime         @default(now())
  updatedAt                                                DateTime         @updatedAt
  //fromUnitPrice                                            Float
  //toUnitPrice                                              Float
  isSell                                                   Boolean
  fromAmount                                               Decimal
  toAmount                                                 Decimal
  closeAtProfit                                            Decimal
  closeAtLoss                                              Decimal
  whenRateIs                                               Decimal
  orderType                                                Int
  status                                                   String           @default("waiting")
  fromAssetId                                              Int
  userId                                                   Int
  toAssetId                                                Int
  FromAsset Asset @relation("AssetToOrder_fromAssetId", fields: [fromAssetId], references: [id])
  //TODO: add this 
  ToAsset   Asset @relation("AssetToOrder_toAssetId", fields: [toAssetId], references: [id])
  User      User  @relation(fields: [userId], references: [id])

  // @@index([fromAssetId], name: "orderFromAssetId")
  // @@index([toAssetId], name: "orderToAssetId")
  @@index([userId], name: "orderToUserId")
}

model User {
  id          Int           @id @default(autoincrement())
  createdAt   DateTime      @default(now())
  updatedAt   DateTime      @updatedAt
  username    String        @unique
  socialId    String?
  email       String        @unique
  password    String?
  loginType   Int
  status      String       @default("active")
  totalWealth Decimal      @default(50000)
  Account     Account[]    @relation("UserAccount")
  Device      Device[]     @relation("UserDevice")
  Transaction Transaction[] @relation("UserTransaction")
  Order       Order[]
  UserLog     UserLog[]
  Wealth     Wealth[] @relation("UserWealth")
  Favorite  Favorite[] @relation("UserFavorite")
}

model Wealth {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now())
  userId    Int
  period    Int
  value     Decimal
  User      User     @relation("UserWealth",fields: [userId], references: [id])

  @@index([userId], name: "wealthUserId")
  @@index([createdAt])
}

model AssetHistory {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now())
  assetId   Int
  period    Int
  value     Decimal
  Asset     Asset     @relation("AssetToHistory",fields: [assetId], references: [id])

  @@index([assetId], name: "assetHistoryUserId")
  @@index([createdAt])
}

model UserLog {
  id        Int      @id @default(autoincrement())
  createdAt DateTime  @default(now())
  userId    Int
  logType   Int?
  content   String?
  duration  Int?
  User      User     @relation(fields: [userId], references: [id])

  @@index([userId], name: "userLogUserId")
}

model Favorite {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now())
  userId    Int
  assetId   Int
  User      User     @relation("UserFavorite",fields: [userId], references: [id])
  Asset     Asset    @relation("AssetFavorite",fields: [assetId], references: [id])

  @@index([userId], name: "favoriteUserId")
}