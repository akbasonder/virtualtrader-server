import { PrismaClient } from '@prisma/client'
import { allForexData } from './json/allForexData'
import moment from 'moment'
import { Logger } from '../util/Logger'
import * as rm from 'typed-rest-client/RestClient'
import { BASE_URL, ACCESS_KEY } from '../graphql/constants'

const logger = new Logger('ForexPriceUpdater')
interface AllForex {
  status: string
  code: number
  msg: string
  response: [
    {
      id: string
      o: string
      h: string
      l: string
      c: string
      a?: string
      b?: string
      ch: string
      cp: string
      t: string
      s: string
      tm: string
    },
  ]
}

const updateForex = async () => {
  const prisma = new PrismaClient()
  try {
    let rest: rm.RestClient = new rm.RestClient('all-crypto', BASE_URL)
    let res: rm.IRestResponse<AllForex> = await rest.get<AllForex>(
      '/api-v3/forex/latest?symbol=all_forex&access_key=' + ACCESS_KEY,
    );

    res.result?.response.map(async (item) => {
      try {
        const index = item.s.indexOf('/USD');
        if (index > 0 && item.c && item.o) {
          logger.log('item is', item)
          const asset = await prisma.asset.findUnique({
            where: {
              symbol: item.s,
            },
          });
          logger.log('asset is', asset);
          if (asset !== null) {
            const assetId = asset.id;
            logger.log('assetId is', assetId);
            prisma.asset.update({
              data: {
                open: parseFloat(item.o),
                high: item.h ? parseFloat(item.h) : 0,
                low: item.l ? parseFloat(item.l) : 0,
                price: parseFloat(item.c),
                ask: item.a ? parseFloat(item.a) : undefined,
                bid: item.b ? parseFloat(item.b) : undefined,
                changeNominal: item.ch ? parseFloat(item.ch) : 0,
                changePercentage: item.cp
                  ? parseFloat(item.cp.substring(0, item.cp.length - 1))
                  : 0,
                timestamp: moment(item.tm, 'YYYY-MM-DD HH:mm:ss').toDate(),
                rank: parseInt(item.id),
              },
              where: {
                id: assetId,
              },
            }).then((res: any) => logger.debug('Asset updated with res', res))
            .catch((err: any) =>
              logger.error('Asset was not updated. Error is', err),
            )
            logger.debug('item was inserted', item);
          }
        }
      } catch (err) {
        logger.error('insert error1 is', err);
        logger.error('problematic record is', item);
      }
    })
  } catch (err) {
    logger.error('insert error2 is', err)
  } finally {
    setTimeout(() => prisma.$disconnect(), 30000);
    logger.info('insert operation finished');
  }
}

updateForex().then((res) => logger.info('updateForex finished'))
.catch((err) => logger.error('updateForex error is', err));

