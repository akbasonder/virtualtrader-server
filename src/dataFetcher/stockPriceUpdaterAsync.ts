import { PrismaClient } from '@prisma/client'
import { allUsStockData } from './json/usStockData'
import moment from 'moment'
import { Logger } from '../util/Logger'

const logger = new Logger('StockPriceUpdater')
//const prisma = new PrismaClient()
const prismaConnections = [
  new PrismaClient(),
  new PrismaClient(),
  new PrismaClient(),
  new PrismaClient(),
  new PrismaClient(),
]
type ID = {
  id : any
}

const updateStock = async () => {
  try {
    allUsStockData.response.map(async (item, index) => {
      try {
        const prisma = prismaConnections[index % 5]
        logger.log('item is', item)
        prisma.assetDefinition
          .findFirst({
            where: {
              symbol: item.s,
              country: item.cty,
              exchange: item.exch,
            },
          })
          .then((assetDefRes: ID) => {
            logger.log('assetDefRes is ', assetDefRes)
            if (assetDefRes && item.c) {
              const assetDefId = assetDefRes.id
              logger.log('assetDefId is', assetDefId)
              prisma.assetValue
                .findFirst({
                  where: {
                    AssetDefinition: { id: assetDefId },
                  },
                })
                .then((assetValueRes: ID) => {
                  logger.log('AssetValueRes is', assetValueRes)
                  if (assetValueRes) {
                    prisma.assetValue
                      .update({
                        data: {
                          high: item.h ? parseFloat(item.h) : 0,
                          low: item.l ? parseFloat(item.l) : 0,
                          price: parseFloat(item.c),
                          changeNominal: item.ch ? parseFloat(item.ch) : 0,
                          changePercentage: item.cp
                            ? parseFloat(
                                item.cp.substring(0, item.cp.length - 1),
                              )
                            : 0,
                          tm: moment(item.tm, 'YYYY-MM-DD HH:mm:ss').toDate(),
                          rank: parseInt(item.id),
                        },
                        where: {
                          id: assetValueRes.id,
                        },
                      })
                      .then((res: any) =>
                        logger.log('AssetValue updated with value', res),
                      )
                      .catch((err: any) =>
                        logger.error('AssetValue update error is', err),
                      )
                  } else {
                    logger.debug('item will be inserted', item)
                    prisma.assetValue
                      .create({
                        data: {
                          high: item.h ? parseFloat(item.h) : 0,
                          low: item.l ? parseFloat(item.l) : 0,
                          price: parseFloat(item.c),
                          changeNominal: parseFloat(item.ch),
                          changePercentage: parseFloat(
                            item.cp.substring(0, item.cp.length - 1),
                          ),
                          tm: moment(item.tm, 'YYYY-MM-DD HH:mm:ss').toDate(), //new Date(parseInt(item.tm)),
                          rank: parseInt(item.id),
                          AssetDefinition: { connect: { id: assetDefId } },
                        },
                      })
                      .then((res: any) =>
                        logger.log('AssetValue inserted with value', res),
                      )
                      .catch((err: any) =>
                        logger.error('AssetValue insert error is', err),
                      )
                  }
                })
                .catch((err: any) => logger.error('AssetValue Find error is', err))
              /*
          if (assetValue) {
            prisma.assetValue.update({
              data: {
                high: item.h ? parseFloat(item.h) : 0,
                low: item.l ? parseFloat(item.l) : 0,
                price: parseFloat(item.c),
                changeNominal: item.ch ? parseFloat(item.ch) : 0,
                changePercentage: item.cp
                  ? parseFloat(item.cp.substring(0, item.cp.length - 1))
                  : 0,
                tm: moment(item.tm, 'YYYY-MM-DD HH:mm:ss').toDate(),
                rank: parseInt(item.id),
              },
              where: {
                id: assetValue.id,
              },
            }).then(res=>logger.log('AssetValue updated with value',res))
            .catch(err=>logger.error('AssetValue update error is',err))
          } else {
            logger.debug('item will be inserted', item)
            prisma.assetValue.create({
              data: {
                high: item.h ? parseFloat(item.h) : 0,
                low: item.l ? parseFloat(item.l) : 0,
                price: parseFloat(item.c),
                changeNominal: parseFloat(item.ch),
                changePercentage: parseFloat(
                  item.cp.substring(0, item.cp.length - 1),
                ),
                tm: moment(item.tm, 'YYYY-MM-DD HH:mm:ss').toDate(), //new Date(parseInt(item.tm)),
                rank: parseInt(item.id),
                AssetDefinition: { connect: { id: assetDefId } },
              },
            }).then(res=>logger.log('AssetValue inserted with value',res))
            .catch(err=>logger.error('AssetValue insert error is',err))
          }*/
              logger.debug('item was inserted', item)
            }
          })
          .catch((err: any) =>
            logger.error('AssetDefinition findFirst result is', err),
          )
      } catch (err) {
        logger.error('insert error1 is', err)
      }
    })
  } catch (err) {
    logger.error('insert error2 is', err)
  } finally {
    //setTimeout(async () => prismaConnections.map((prisma,index)=> prisma.$disconnect()), 120000);
    prismaConnections.map((prisma, index) => prisma.$disconnect())
    logger.info('insert operation finished')
  }
}

updateStock()
