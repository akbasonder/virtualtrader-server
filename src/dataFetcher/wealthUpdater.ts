import { PrismaClient } from '@prisma/client'
import moment from 'moment'
import { Logger } from '../util/Logger'
import { PERIOD_MARKER } from './common'

const logger = new Logger('WealthUpdater')

type Account = { 
  amount: number; 
  averagePrice: number 
}
const updateWealth = async () => {
  const prismaConnections = [
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
  ]

  //moment(item.tm, 'YYYY-MM-DD HH').toDate()

  try {
    const startTime = new Date()
    const users = await prismaConnections[0].user.findMany({
      where: {
        status: 'active',
      },
      include: {
        Account: true,
      },
    })
    const hour = moment().hour()
    logger.log('hour is', hour)
    const dayOfMonth = moment().date()
    const dayOfYear = moment().dayOfYear()
    logger.log('users is', users)
    //users.map((user, index) =>
    for (let index = 0; index < users.length; index++) {
      const user = users[index]
      const prisma = prismaConnections[index % 5]
      logger.debug('user isss', user)
      let period = PERIOD_MARKER.HOUR
      logger.debug('period is', period)
      let totalWealth = 0
      user.Account.map((account: Account) => {
        logger.log('account is', account)
        totalWealth += account.amount * account.averagePrice
      })
      logger.debug('totalWealth is', totalWealth)
      let timePastHour = 0
      try {
        const lastWealth = await prisma.wealth.findFirst({
          where: {
            userId: user.id,
          },
          orderBy: {
            createdAt: 'desc',
          },
        })
        logger.debug('lastWealth', lastWealth)
        if (lastWealth) {
          timePastHour = (startTime.getTime() - lastWealth.createdAt) / 3600000
          logger.debug('timePastHour is', timePastHour)
          if (timePastHour < 0.5) {
            continue
          }
        }
      } catch (err) {
        logger.error(
          'Wealth findFirst could not be found for userId=' + user.id,
        )
      }

      if (timePastHour > hour + 0.5) {
        if (dayOfMonth === 1) {
          period = PERIOD_MARKER.MONTH
        } else {
          if (dayOfYear % 3 === 0) {
            //TODO: showing three_days data can be problematic
            period = PERIOD_MARKER.THREE_DAYS
          } else {
            period = PERIOD_MARKER.DAY
          }
        }
      }

      prisma.wealth
        .create({
          data: {
            userId: user.id,
            period,
            value: totalWealth,
          },
        })
        .then((res: any) => logger.debug('wealth create with result=', res))
        .catch((err: any) =>
          logger.error('wealth could not created. Error is', err),
        )

      prisma.user
        .update({
          where: {
            id: user.id,
          },
          data: {
            totalWealth,
          },
        })
        .then((res: any) => logger.debug('wealth create with result=', res))
        .catch((err: any) =>
          logger.error('wealth could not created. Error is', err),
        )
    } //)
  } catch (err) {
    logger.error('insert error1 is', err)
  } finally {
    setTimeout(
      () =>
        prismaConnections.map(async (prisma, index) => {
          prisma.$disconnect()
          logger.info('connection ' + index + ' disconnected')
        }),
      10000,
    )
    logger.info('insert operation finished')
  }
}

updateWealth()
  .then((res) => logger.info('updateWealth finished'))
  .catch((err) => logger.error('updateWealth error is', err))
