import { PrismaClient } from '@prisma/client'
import { allForex } from './json/forexDefinitionData'
import { allUsStock } from './json/usStockDefinition1'
import { allCrypto } from './json/cryptoDefinition2'
import { Logger } from '../util/Logger'

const logger = new Logger('DefinitionInsertor')

const insertForex = async () => {
  const prismaConnections = [
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
  ]
  try {
    logger.info('insertForex started')
    allForex.response.map(async (item, prismaIndex) => {
      try {
        const index = item.symbol.indexOf('/USD')
        if (index > 0) {
          const prisma = prismaConnections[prismaIndex % 5]
          await prisma.asset.create({
            data: {
              rank: parseInt(item.id),
              symbol: item.symbol,
              name: item.name,
              category: 'FOREX',
            },
          })
          logger.log('item inserted', item);
        }
      } catch (err) {
        logger.error('insertForex error1 is', err);
        logger.error('problematic forex is', item);
      }
    })
  } catch (err) {
    logger.error('insertForex error2 is', err)
  } finally {
    for (let i = 0; i < 5; i++) {
      const prisma = prismaConnections[i]
      await prisma.$disconnect()
    }
    logger.log('insertForex finished')
  }
}

const insertUsStock = async () => {
  const prismaConnections = [
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
  ]
  try {
    logger.info('insertUsStock started')
    allUsStock.response.map(async (item, prismaIndex) => {
      if (item.exch) {
        const prisma = prismaConnections[prismaIndex % 5]
        try {
          await prisma.asset.create({
            data: {
              rank: parseInt(item.id),
              symbol: item.short_name + '/' + item.exch,
              name: item.name,
              category: 'STOCK',
              country: item.country,
              exchange: item.exch,
            },
          })
        } catch (err) {
          logger.error('insertUsStock error1 is', err)
          logger.error('problematic stock is', item)
        }
        //logger.log("item inserted", item);
      }
    })
  } catch (err) {
    logger.error('insertUsStock error2 is', err)
  } finally {
    for (let i = 0; i < 5; i++) {
      const prisma = prismaConnections[i];
      await prisma.$disconnect();
    }
    logger.log('insertUsStock finished')
  }
}

const insertCrypto = async () => {
  const prismaConnections = [
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
  ]
  try {
    logger.info('insertCrypto started')
    allCrypto.response.map(async (item, prismaIndex) => {
      const index = item.symbol.indexOf('/USD')
      if (index > 0) {
        const prisma = prismaConnections[prismaIndex % 5]
        try {
          await prisma.asset.create({
            data: {
              rank: parseInt(item.id),
              symbol: item.symbol,
              name: item.name,
              category: 'CRYPTO',
            },
          })
        } catch (err) {
          logger.error('insertCrypto error1 is', err)
          logger.error('problematic crypto is', item)
        }
        //console.log("item inserted", item);
      }
    })
  } catch (err) {
    logger.error('insertCrypto error2 is', err)
  } finally {
    for (let i = 0; i < 5; i++) {
      const prisma = prismaConnections[i];
      await prisma.$disconnect();
    }
    logger.log('insertCrypto finished')
  }
}

const insertCurrency = async () => {
  const prisma = new PrismaClient();
  try {
    logger.info('insertCurrency started')
    await prisma.asset.create({
      data: {
        rank: 1,
        symbol: 'USD',
        name: 'US Dollar',
        category: 'CURRENCY',
        country: 'united-states',
      },
    })
  } catch (err) {
    logger.error('insertCurrency error is', err)
  } finally {
    await prisma.$disconnect();
    logger.log('insertCurrency finished')
  }
}

/*insertForex()
  .then((res) => logger.info('then insertForex'))
  .catch((err) => logger.error('error insertForex'))
insertUsStock()
  .then((res) => logger.info('then insertUsStock'))
  .catch((err) => logger.error('error insertUsStock'))
insertCrypto()
  .then((res) => logger.info('then insertCrypto'))
  .catch((err) => logger.error('error insertCrypto'))*/
  insertCurrency()
  .then((res) => logger.info('then insertCurrency'))
  .catch((err) => logger.error('error insertCurrency'))