import { PrismaClient } from '@prisma/client'
//import { allUsStockData } from './json/usStockData'
import moment from 'moment'
import { Logger } from '../util/Logger'
import * as rm from 'typed-rest-client/RestClient'
import { BASE_URL, ACCESS_KEY } from '../graphql/constants'
import { createAssetHistory } from './common'

const logger = new Logger('StockPriceUpdater')
//const prisma = new PrismaClient()
interface AllUsStock {
  status: string
  code: number
  msg: string
  response: [
    {
      id: string
      o: string
      h: string
      l: string
      c: string
      ch: string
      cp: string
      t: string
      s: string
      tm: string
      cty: string
      exch: string
    },
  ]
}
const updateStock = async () => {
  logger.debug('updateStock started')
  const prismaConnections = [
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
    new PrismaClient(),
  ]

  try {
    let rest: rm.RestClient = new rm.RestClient('all-crypto', BASE_URL)
    let res: rm.IRestResponse<AllUsStock> = await rest.get<AllUsStock>(
      '/api-v3/stock/latest?country=united-states&access_key=' + ACCESS_KEY,
    )
    res?.result?.response.map(async (item, index) => {
      logger.log('item is', item)
      if (item.c && item.exch && item.s) {
        const prisma = prismaConnections[index % prismaConnections.length]
        const asset = await prisma.asset.findUnique({
          where: {
            symbol: item.s + '/' + item.exch,
          },
        })
        logger.log('asset is', asset)
        if (asset !== null) {
          const assetId = asset.id
          logger.log('assetId is', assetId)
          const price = parseFloat(item.c)
          prisma.asset
            .update({
              data: {
                high: item.h ? parseFloat(item.h) : 0,
                low: item.l ? parseFloat(item.l) : 0,
                price,
                ask: price * 1.005,
                bid: price * 0.995,
                changeNominal: item.ch ? parseFloat(item.ch) : 0,
                changePercentage: item.cp
                  ? parseFloat(item.cp.substring(0, item.cp.length - 1))
                  : 0,
                timestamp: moment(item.tm, 'YYYY-MM-DD HH:mm:ss').toDate(),
                rank: parseInt(item.id),
              },
              where: {
                id: assetId,
                //symbol: item.s + '/' + item.exch,
              },
            })
            .then((res : any) => logger.debug('Asset updated with res', res))
            .catch((err: any) =>
              logger.error('Asset was not updated. Error is', err),
            )
        }
      }
    })
  } catch (err) {
    logger.error('insert error2 is', err)
  } finally {
    setTimeout(
      () =>
        prismaConnections.map(async (prisma, index) => {
          prisma.$disconnect()
          logger.info('connection ' + index + ' disconnected')
        }),
      60000,
    )
    logger.info('insert operation finished')
  }
}

updateStock()
  .then((res) => logger.info('updateStock finished'))
  .catch((err) => logger.error('updateStock error is', err))

/*const testAssetHistory = () => {
  logger.debug('testAssetHistory started')
  const prisma = new PrismaClient();
  createAssetHistory(prisma,7516, 123);
  setTimeout(() => {prisma.$disconnect(); logger.debug('prisma disconnected')},10000);
}
testAssetHistory();*/
