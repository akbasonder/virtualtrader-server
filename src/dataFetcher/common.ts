import moment from 'moment'
import { Logger } from '../util/Logger'

const logger = new Logger('Common')
export enum PERIOD_MARKER {
  'HOUR' = 1, // for daily graph
  'DAY' = 2, // for weekly graph
  'THREE_DAYS' = 3, //for monthly graph
  'MONTH' = 4, // for yearly graph
}

export const createAssetHistory = async (
  prisma: any,
  assetId: number,
  value: number,
) => {
  const startTime = new Date()
  const hour = moment().hour()
  const dayOfMonth = moment().date()
  const dayOfYear = moment().dayOfYear()
  let period = PERIOD_MARKER.HOUR
  logger.debug('period is', period)
  let timePastHour = 0
  try {
    const assetHistory = await prisma.assetHistory.findFirst({
      where: {
        assetId,
      },
      orderBy: {
        createdAt: 'desc',
      },
    })
    logger.debug('assetHistory', assetHistory)
    if (assetHistory) {
      timePastHour = (startTime.getTime() - assetHistory.createdAt) / 3600000
      logger.debug('timePastHour is', timePastHour)
      if (timePastHour < 0.5) {
        return
      }
    }
  } catch (err) {
    logger.error('AssetHistory could not be found for assetId=' + assetId)
  }

  if (timePastHour > hour + 0.5) {
    if (dayOfMonth === 1) {
      period = PERIOD_MARKER.MONTH
    } else {
      if (dayOfYear % 3 === 0) {
        //TODO: showing three_days data can be problematic
        period = PERIOD_MARKER.THREE_DAYS
      } else {
        period = PERIOD_MARKER.DAY
      }
    }
  }

  prisma.assetHistory
    .create({
      data: {
        assetId,
        period,
        value,
      },
    })
    .then((res: any) => logger.debug('wealth create with result=', res))
    .catch((err: any) =>
      logger.error('wealth could not created. Error is', err),
    )
}
