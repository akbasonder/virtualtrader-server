
import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

//export interface Context {
//  prisma: PrismaClient
//}

//export function createContext(): Context {
//  return { prisma }
//}

export interface Context {
  prisma: PrismaClient
  req: any
}

export function createContext(req: Context) {
  return {
    ...req,
    prisma,
  }
}