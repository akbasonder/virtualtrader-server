import { Context } from '../context'
import { APP_SECRET, TOKEN_KEY } from '../constants'
import { sign } from 'jsonwebtoken'
import * as nodemailer from 'nodemailer'
import { Logger } from '../../util/Logger'
import md5 from 'md5'
import randomstring from 'randomstring'
export const transporter = nodemailer.createTransport({
  //smtp://smtp.gmail.com:587/?requireTLS=true
  //service:'Godaddy',
  host: 'smtp.office365.com', //"smtp.ethereal.email",// 'smtpout.secureserver.net',//'smtp.office365.com',//'smtp.domain.com',
  port: 587, //587,//465,
  //secureConnection: true,  //true for 465 port, false for other ports
  secure: false,
  requireTLS: true,
  auth: {
    user: 'no-reply@waidapp.com',
    pass: 'Nswer3S_sU1s8',
  },
  tls: {
    ciphers: 'SSLv3',
  },
})

const logger = new Logger('Auth Schema')

export const typeDefs = `
type AuthPayload {
  token: String!
}

type OperationResult {
  code: Int!
}

type Query {
  getToken(tokenKey:String!): AuthPayload!  
}

type Mutation {
  forgotPassword(tokenKey:String!, email:String!): Boolean!
  trade(tokenKey: String!,
    fromAmount: Float!,
    fromAssetSymbol: String!,
    toAmount: Float!,
    isSell: Boolean!,
    toAssetSymbol: String!,
    userId: Int!): OperationResult!
}
`
enum CODES {
  'SUCCESS' = 1,
  'FROM_ACOUNT_NOT_FOUND' = 2,
}

export const resolvers = {
  Query: {
    getToken: async (parent: any, { tokenKey }: any, ctx: Context) => {
      if (tokenKey !== TOKEN_KEY) {
        throw new Error('INVALID TOKEN KEY')
        //return {
        //  token: 'INVALID TOKEN_KEY',
        //}
      }
      const token = sign({ userId: tokenKey }, APP_SECRET);
      return {
        token,
      }
    },
  },

  Mutation: {
    forgotPassword: async (
      parent: any,
      { tokenKey, email }: any,
      ctx: Context,
    ) => {
      if (tokenKey !== TOKEN_KEY) {
        throw new Error('INVALID TOKEN KEY')
      }
      const password = randomstring.generate(8)
      logger.debug('generated password=', password)
      try {
        await ctx.prisma.user.update({
          data: { password: md5(password) },
          where: { email },
        })
        const mailOptions = {
          from: '"no-reply" <no-reply@waidapp.com>', // sender address
          to: email, // list of receivers
          subject: 'Virtual Trader new password', // Subject line
          text: 'Your new password : ' + password, //'Hello world?', // plain text body
        }
        //logger.debug('updateRes is', updateRes);
        try {
          transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
              logger.error(error)
              throw new Error(`Sending mail failed for email: ${email}`)
            } else {
              //res.status(200).send({success: true});
            }
          })
        } catch (err) {
          logger.debug('sending mail error', err)
        }
      } catch (err) {
        logger.error('forgotPassword error is', err)
        return false
      }
      return true
    },

    trade: async (
      parent: any,
      {
        tokenKey,
        fromAmount,
        fromAssetSymbol,
        isSell,
        toAmount,
        toAssetSymbol,
        userId,
      }: any,
      ctx: Context,
    ) => {
      if (tokenKey !== TOKEN_KEY) {
        logger.debug('trade tokenKey is NOOOOTTT correct');
        throw new Error('INVALID TOKEN KEY')
      }
      logger.debug('trade tokenKey is correct');
      const fromAccount = await ctx.prisma.account.findFirst({
        where: {
          userId,
          Asset:{ symbol : fromAssetSymbol}
        },
        //select: {
        //  Asset:{ where: {symbol : fromAssetSymbol}}
        //}
      });
      logger.debug('fromAccount is', fromAccount);
      if(!fromAccount){
        return {
          code: CODES.FROM_ACOUNT_NOT_FOUND,
        }
      }
      const toAccount = await ctx.prisma.account.findFirst({
        where: {
          userId,
          Asset:{ symbol : toAssetSymbol}
        },
        include: {
          Asset: true,
        },
      });
      logger.debug('toAccount is', toAccount);

     
      const changeFrom = ctx.prisma.account.update({
            where: { id: fromAccount.id },
            data: {
              amount: fromAccount.amount - fromAmount,
              //averagePrice: fromAveragePrice,
            },
          })
      logger.log('trade changeFrom OK');

      const changeTo = toAccount
        ? ctx.prisma.account.update({
            where: { id: toAccount.id },
            data: { 
              amount: toAccount.amount + toAmount, 
              averagePrice:  (toAccount.amount * toAccount.averagePrice + toAmount * (isSell ? toAccount.Asset.bid: toAccount.Asset.ask)) + (toAccount.amount + toAmount)
            },
          })
        : ctx.prisma.account.create({
            data: {
              amount: toAmount,
              averagePrice: toAmount/fromAmount,
              Asset: {
                connect: { symbol: toAssetSymbol },
              },
              User: { connect: { id: userId } },
            },
          });

      logger.log('trade changeTo OK');
      const transaction = ctx.prisma.transaction.create({
        data: {
          FromAsset: { connect: { symbol: fromAssetSymbol } },
          //fromUnitPrice: fromAveragePrice ? fromAveragePrice : 1,
          fromAmount,
          ToAsset: { connect: { symbol: toAssetSymbol } },
          //toUnitPrice: toAveragePrice ? toAveragePrice : 1,
          toAmount,
          isSell,
          transactionType: 1,
          User: { connect: { id: userId } },
        },
      })
      logger.log('trade transaction OK')

      await ctx.prisma.$transaction([changeFrom, changeTo, transaction])
      logger.log('trade PrismaTransaction OK')
      return {
        code: CODES.SUCCESS,
      }
    },
  },
}
