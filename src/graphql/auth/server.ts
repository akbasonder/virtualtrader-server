//import { ApolloServer } from "apollo-server";
import { ApolloServer } from 'apollo-server-express'
import { typeDefs, resolvers } from "./schema";
import { createContext } from "../context";
import { AUTH_PORT,SSL_KEY_PATH,SSL_CERT_PATH } from "../constants";
import fs from 'fs';
import https from 'https';
import express from 'express';
import {Logger} from '../../util/Logger'

const logger = new Logger('Auth Server');

const apollo = new ApolloServer({ 
    typeDefs, 
    resolvers, 
    context: createContext,

})
const app = express();
apollo.applyMiddleware({ app });

const server = https.createServer(
  {
    key: fs.readFileSync(SSL_KEY_PATH),
    cert: fs.readFileSync(SSL_CERT_PATH)
  },
  app
);

server.listen({ port: AUTH_PORT}, () =>
  logger.info(
    '🚀 Server ready at',
    `https://localhost:${AUTH_PORT}${apollo.graphqlPath}`
  )
)
/*.listen(
  { ...options,
    https: {
      key: fs.readFileSync(SSL_KEY_PATH),
      cert: fs.readFileSync(SSL_CERT_PATH)
    } },
  () =>
    logger.log(
      `🚀 Server ready at: http://localhost:4000\n⭐️ See sample queries: http://pris.ly/e/ts/graphql-sdl-first#using-the-graphql-api`
    )
);
*/