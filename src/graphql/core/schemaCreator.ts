// @ts-nocheck
import "reflect-metadata";
import { PrismaClient } from "@prisma/client";
import { ApolloServer } from "apollo-server";
import { resolvers, relationResolvers, crudResolvers } from "../../../prisma/generated/type-graphql";
import { buildSchema } from "type-graphql";

 const main = async() =>{
    console.log('schema creation started');
    const schema = await buildSchema({
        resolvers,
        // automatically create `schema.gql` file with schema definition in project's working directory
        // emitSchemaFile: true,
        // or create the file with schema in selected path
        //emitSchemaFile: path.resolve(__dirname, "__snapshots__/schema/schema.gql"),
        // or pass a config object
        emitSchemaFile: {
          path: __dirname + "/schema.graphql",
          commentDescriptions: true,
          sortedSchema: false, // by default the printed schema is sorted alphabetically
        },
        validate: false,
      });
      console.log('schema creation  finished');
 }

 main().catch(err=>console.log('error is', err));