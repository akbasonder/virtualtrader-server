//@ts-nocheck
//import { ApolloServer } from "apollo-server";
import { ApolloServer, gql } from 'apollo-server-express'
import { resolvers } from "./resolver";
import { createContext } from "../context";
import { CORE_PORT,APP_SECRET,ENDPOINT,SSL_KEY_PATH,SSL_CERT_PATH } from "../constants";
import { AuthenticationError } from 'apollo-server-core';
import jwt from 'jsonwebtoken';
import fs from 'fs';
import https from 'https';
import express from 'express';
import {Logger} from '../../util/Logger'
const logger = new Logger('Core Server');

const apollo = new ApolloServer({ 
  typeDefs:  gql(fs.readFileSync(__dirname.concat('/schema.graphql'), 'utf8')),//'./src/graphql/core/schema.gql',//'src/graphql/core/schema.graphql',
  resolvers, 
  context: ({req})=>{
    // Note! This example uses the `req` object to access headers,
    // but the arguments received by `context` vary by integration.
    // This means they will vary for Express, Koa, Lambda, etc.!
    //
    // To find out the correct arguments for a specific integration,
    // see the `context` option in the API reference for `apollo-server`:
    // https://www.apollographql.com/docs/apollo-server/api/apollo-server/
 
    // Get the user token from the headers.
    const token = req.headers.authorization || '';
    logger.debug('header token is',token);
    try{
      const verify = jwt.verify(token.substring(7), APP_SECRET); 
      logger.debug('verify', verify);
    }catch (e) {
      logger.error('auth error occured',e);
      throw new AuthenticationError(e.message);    
    }    
    return createContext(req) ;
  },  

})
const app = express();

apollo.applyMiddleware({ app });

const server = https.createServer(
{
  key: fs.readFileSync(SSL_KEY_PATH),
  cert: fs.readFileSync(SSL_CERT_PATH)
},
app
);

server.listen({ port: CORE_PORT}, () =>
logger.info(
  '🚀 Server ready at',
  `https://localhost:${CORE_PORT}${apollo.graphqlPath}`
)
)

