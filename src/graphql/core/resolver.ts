import { Context } from '../context'
import {Logger} from '../../util/Logger'
import { transporter } from '../auth/schema';

const logger = new Logger('Resolver');

export const resolvers = {
    Query: {
      user:  (parent: any, args: any, ctx: Context) => {      
        const user = ctx.prisma.user.findUnique(args);
        logger.debug('user1',user);
        return user;
      },
      users: (parent: any, args: any, ctx: Context) =>{
        logger.debug('users args=', args);
        return ctx.prisma.user.findMany(args);
      },
      account:  (parent: any, args: any, ctx: Context) => {     
        logger.debug('account query started') ;
        const account = ctx.prisma.account.findUnique(args);
        logger.debug('account',account);
        return account;
      },
      accounts: (parent: any, args: any, ctx: Context) => {
        logger.debug('accounts args=', args);
        return ctx.prisma.account.findMany({...args ,include: {Asset : true}});
      },
      asset: (parent: any, args: any, ctx: Context) => {      
        return ctx.prisma.asset.findUnique(args);
      },
      assets: (parent: any, args: any, ctx: Context) => {
        logger.debug('assetDefinition args=', args);
        return ctx.prisma.asset.findMany(args);
      },
      transaction: (parent: any, args: any, ctx: Context) => {      
        return ctx.prisma.transaction.findUnique(args);
      },
      transactions: (parent: any, args: any, ctx: Context) => {
        logger.debug('transactions args=', args);
        return ctx.prisma.transaction.findMany({...args ,include: {FromAsset : true , ToAsset: true}});
      },
      order: (parent: any, args: any, ctx: Context) => {      
        return ctx.prisma.order.findUnique(args);
      },
      orders: (parent: any, args: any, ctx: Context) => {
        logger.debug('orders args=', args);
        return ctx.prisma.order.findMany({...args ,include: {FromAsset : true , ToAsset: true}});
      },
      findUniqueWealth: (parent: any, args: any, ctx: Context) => {      
        return ctx.prisma.wealth.findUnique(args);
      },
      findManyWealth: (parent: any, args: any, ctx: Context) => {
        logger.debug('wealths args=', args);
        return ctx.prisma.wealth.findMany(args);
      },
      /*assetValue: (parent: any, args: any, ctx: Context) => {      
        return ctx.prisma.assetValue.findUnique({...args ,include: {AssetDefinition : true}});
      },
      assetValues: (parent: any, args: any, ctx: Context) => {
        logger.debug('assetValue args1=', args);
        return ctx.prisma.assetValue.findMany({...args ,include: {AssetDefinition : true}});
      },*/
    },

    Mutation: {   
        createUser: async(parent: any, args : any, ctx: Context) => {
          logger.debug('createUser args', args);
          const result = await ctx.prisma.user.create(args); //{data: args.data}
          logger.debug('email is', args.data.email);
          logger.debug('createUser result is', result);
          const mailOptions = {
            from: 'no-reply@waidapp.com',//'"no-reply" <no-reply@waidapp.com>', // sender address
            to: args.data.email, // list of receivers
            subject: 'Welcome to Virtual Trader', // Subject line
            text: 'Nice to join '//'Hello world?', // plain text body
            //html: '<b>Hello world?</b>' // html body
          };
          transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
              logger.error('sending mail failed',error);
              //res.status(400).send({success: false})
              throw new Error(`Sending mail failed for email: ${args.data.email}`)
            } else {
              //res.status(200).send({success: true});
            }
          });
          return result;
        },
        updateUser: (parent: any, args : any, ctx: Context) =>{
            logger.debug('updateUser args', args);
            return ctx.prisma.user.update(args);
        },   

        /*createAsset: async(parent: any, args : any, ctx: Context) => {
          logger.debug('createAsset args', args);
          return ctx.prisma.asset.create(args); //{data: args.data}          
        },*/    
      },
  }